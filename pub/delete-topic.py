from google.cloud import pubsub_v1

project_id = "apna-staging"

def delete_topic(topic_id: str) -> None:
    """Deletes an existing Pub/Sub topic."""

    publisher = pubsub_v1.PublisherClient()
    topic_path = publisher.topic_path(project_id, topic_id)

    publisher.delete_topic(request={"topic": topic_path})

    print(f"Topic deleted: {topic_path}")

delete_topic("local-topic")