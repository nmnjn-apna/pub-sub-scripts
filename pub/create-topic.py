from google.cloud import pubsub_v1

project_id = "apna-staging"

def create_topic(topic_id: str) -> None:
    """Create a new Pub/Sub topic."""

    publisher = pubsub_v1.PublisherClient()
    topic_path = publisher.topic_path(project_id, topic_id)

    topic = publisher.create_topic(request={"name": topic_path})

    print(f"Created topic: {topic.name}")


create_topic("local-topic")
