from google.cloud import pubsub_v1

project_id = "apna-staging"

def list_topics() -> None:
    """Lists all Pub/Sub topics in the given project."""

    publisher = pubsub_v1.PublisherClient()
    project_path = f"projects/{project_id}"

    for topic in publisher.list_topics(request={"project": project_path}):
        print(topic)

list_topics()