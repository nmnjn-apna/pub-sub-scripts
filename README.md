# Pub/Sub scripts


### Steps to emulate pub/sub locally

Open a terminal window and run a local pub-sub emulator
```
gcloud beta emulators pubsub start --host-port=localhost:8085
```

_Important:-_ gcloud cli and java sdk needs to be installed.

---

Open another terminal window and perform the following steps for pub/sub setup.

0. Clone the repo and `cd` into folder.

1. Create and activate virtual environment
```
python3 -m venv .venv
source .venv/bin/activate
```

2. Install packages
```
pip install -r requirements.txt
```

3. Initialise pub-sub environment
```
$(gcloud beta emulators pubsub env-init)
```

4. [Pub] Create a topic
```
python pub/create-topic.py
```

5. [Sub] Create a subscription
```
python sub/create-push-subscription.py
```
<br/>
_this will create a subscription that will push the event to the specified endpoint. use create-pull-subscription.py if you want a pull type subscription._

6. [Pub] Publish message to topic
```
python pub/publish-message.py
```

**Important:-** edit the variables in the python file as needed. 

