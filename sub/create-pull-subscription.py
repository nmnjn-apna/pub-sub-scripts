from google.cloud import pubsub_v1

project_id = "apna-staging"
topic_id = "local-topic"
subscription_id = "local-subscription"

def create_pull_subscription() -> None:
    """Create a new pull subscription on the given topic."""

    publisher = pubsub_v1.PublisherClient()
    subscriber = pubsub_v1.SubscriberClient()
    topic_path = publisher.topic_path(project_id, topic_id)
    subscription_path = subscriber.subscription_path(project_id, subscription_id)

    # Wrap the subscriber in a 'with' block to automatically call close() to
    # close the underlying gRPC channel when done.
    with subscriber:
        subscription = subscriber.create_subscription(
            request={"name": subscription_path, "topic": topic_path}
        )

    print(f"Subscription created: {subscription}")


create_pull_subscription()