from google.cloud import pubsub_v1

project_id = "apna-staging"

def list_subscriptions_in_project() -> None:
    """Lists all subscriptions in the current project."""

    subscriber = pubsub_v1.SubscriberClient()
    project_path = f"projects/{project_id}"

    # Wrap the subscriber in a 'with' block to automatically call close() to
    # close the underlying gRPC channel when done.
    with subscriber:
        for subscription in subscriber.list_subscriptions(
            request={"project": project_path}
        ):
            print(subscription.name)

list_subscriptions_in_project()