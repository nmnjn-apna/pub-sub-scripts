from google.cloud import pubsub_v1

project_id = "apna-staging"

def delete_subscription(subscription_id: str) -> None:
    """Deletes an existing Pub/Sub topic."""

    subscriber = pubsub_v1.SubscriberClient()
    subscription_path = subscriber.subscription_path(project_id, subscription_id)

    # Wrap the subscriber in a 'with' block to automatically call close() to
    # close the underlying gRPC channel when done.
    with subscriber:
        subscriber.delete_subscription(request={"subscription": subscription_path})

    print(f"Subscription deleted: {subscription_path}.")

delete_subscription("my-subscription")